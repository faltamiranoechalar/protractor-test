"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ClassDemo = /** @class */ (function () {
    function ClassDemo(uName) {
        this.userName = "DefaultUserName" + uName;
    }
    Object.defineProperty(ClassDemo.prototype, "getFirstName", {
        get: function () {
            return this.firstName;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ClassDemo.prototype, "setFirstName", {
        set: function (v) {
            this.firstName = v;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ClassDemo.prototype, "getLastName", {
        get: function () {
            return this.lastName;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ClassDemo.prototype, "setLastName", {
        set: function (v) {
            this.lastName = v;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ClassDemo.prototype, "getUserName", {
        get: function () {
            return this.userName;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ClassDemo.prototype, "setUserName", {
        set: function (v) {
            this.userName = v;
        },
        enumerable: true,
        configurable: true
    });
    return ClassDemo;
}());
exports.ClassDemo = ClassDemo;
