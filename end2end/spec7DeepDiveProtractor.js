"use strict";
exports.__esModule = true;
var protractor_1 = require("protractor");
describe('Spec 7 Deep drive into Protractor - ', function () {
    xit("40. Should use chain locators", function () {
        function AddNumbers(a, b) {
            protractor_1.element(protractor_1.by.model("first")).sendKeys(a);
            protractor_1.element(protractor_1.by.model("second")).sendKeys(b);
            protractor_1.element(protractor_1.by.id("gobutton")).click();
        }
        protractor_1.browser.get("http://localhost:3456/", 5000);
        AddNumbers(1, 0);
        AddNumbers(2, 0);
        AddNumbers(3, 0);
        AddNumbers(4, 0);
        AddNumbers(5, 0);
        protractor_1.element.all(protractor_1.by.repeater("result in memory")).each(function (item) {
            item.element(protractor_1.by.xpath("./td[3]")).getText().then(function (resultText) {
                console.log(resultText);
            });
        });
    });
    it("46. Handling list of dropdown options with example", function () {
        function AddNumbers(operA, operB, oper) {
            var numOpera = 0;
            protractor_1.element(protractor_1.by.model("first")).sendKeys(operA);
            protractor_1.element(protractor_1.by.model("second")).sendKeys(operB);
            protractor_1.element.all(protractor_1.by.tagName("option")).each(function (operItem) {
                operItem.getAttribute("value").then(function (operValue) {
                    if (operValue == oper) {
                        operItem.click();
                    }
                });
            });
            protractor_1.element(protractor_1.by.id("gobutton")).click();
        }
        protractor_1.browser.get("http://localhost:3456/", 5000);
        AddNumbers(1, 0, "ADDITION");
        AddNumbers(2, 0, "MULTIPLICATION");
        AddNumbers(3, 0, "SUBTRACTION");
        protractor_1.element.all(protractor_1.by.repeater("result in memory")).each(function (item) {
            item.element(protractor_1.by.xpath("./td[3]")).getText().then(function (resultText) {
                console.log(resultText);
            });
        });
    });
});
