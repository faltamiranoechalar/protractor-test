// este ciclo imprime bien porque LET es sincrono
for(let i=0;i<5;i++){
    setTimeout( function() { console.log(i) },100*i)
}

let myNumber:number = 10
let myString:string = "my test"
let myFlag:boolean = true
let myArr:Array<number> = [1,2,3]