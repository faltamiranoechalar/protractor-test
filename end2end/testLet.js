var _loop_1 = function (i) {
    setTimeout(function () { console.log(i); }, 100 * i);
};
// este ciclo imprime bien porque LET es sincrono
for (var i = 0; i < 5; i++) {
    _loop_1(i);
}
var myNumber = 10;
var myString = "my test";
var myFlag = true;
var myArr = [1, 2, 3];
