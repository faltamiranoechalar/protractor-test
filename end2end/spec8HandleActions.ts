import { browser, element, by, ElementFinder, ExpectedConditions } from "protractor";

describe('Spec 8 Handle actions - ', () => {
    var until = ExpectedConditions

    xit(("48. Actions and its importance"), () => {
        browser.waitForAngularEnabled(false);
        browser.get("https://rahulshettyacademy.com/AutomationPractice/", 5000);
        browser.actions().mouseMove(element(by.id("mousehover"))).perform();
        browser.actions().mouseMove(element(by.linkText("Top"))).click().perform().then(() => {
            browser.sleep(3000); //sleep to see actions
        });
        browser.actions().mouseMove(element(by.id("mousehover"))).perform();
        browser.actions().mouseMove(element(by.linkText("Reload"))).click().perform();
        browser.sleep(3000);
    });

    xit(("49. Handling autocomplete dropdowns"), () => {
        browser.get("https://material.angularjs.org/latest/demo/autocomplete",5000);
        element(by.model("$mdAutocompleteCtrl.scope.searchText")).sendKeys("Ma");

        element.all(by.xpath(".//ul[@class='md-autocomplete-suggestions']/li")).each((item) => {
            item.getText().then((itemText) => {
                if(itemText=="Maryland"){
                    item.click();
                }
            })
        })
        expect(element(by.model("$mdAutocompleteCtrl.scope.searchText")).getAttribute("value")).toMatch("Maryland");
        browser.sleep(3000);
    });

    it(("50. Handling child windows"), () => {
        browser.get("https://posse.com/", 5000);
        element.all(by.xpath(".//*[@class='location']")).each((cityCard) => {
            cityCard.getText()
            .then((cityName) => {
                console.log(cityName);
                if (cityName == "LONDON"){
                    cityCard.click().then(() => {
                        browser.getAllWindowHandles().then((windowHandles) => {
                            browser.switchTo().window(windowHandles[1]);
                        })
                    })
                }
            })
        });
    })

    xit(("49. Handling autocomplete dropdowns"), () => {
        browser.get("https://material.angularjs.org/latest/demo/autocomplete", 5000);
        element(by.model("$mdAutocompleteCtrl.scope.searchText")).sendKeys("Ma");
        element.all(by.xpath(".//ul[@class='md-autocomplete-suggestions']/li")).
            each((item) => {
                item.getText().then((itemText) => {
                    if (itemText == "Maryland") {
                        item.click()
                    }
                })
            })
        expect(element(by.model("$mdAutocompleteCtrl.scope.searchText")).getAttribute("value")).toMatch("Maryland");
        browser.sleep(3000)
    })

    xit("50. Switch to new tabs", () => {
        browser.get("http://posse.com/");
        element(by.xpath(".//span[@class='location' and contains(text(),'LONDON')]")).click()
        
        browser.wait(until.presenceOf(element(by.xpath(".//a[@class='makeInstrumented' and @ng-href]//span[text()='Borough Market']"))), 4000)

        element(by.xpath(".//a[@class='makeInstrumented' and @ng-href]//span[text()='Borough Market']")).click().then(() => {
            browser.getAllWindowHandles().then((windowHandles) => {
                browser.getTitle().then((title) => {
                    console.log("Title before:" + title + "------------------------");
                })
                browser.switchTo().window(windowHandles[1]);
                browser.getTitle().then((title) => {
                    console.log("Title after:" + title+ "------------------------");
                })
                browser.switchTo().window(windowHandles[0]);
                browser.getTitle().then((title) => {
                    console.log("Title final:" + title+ "------------------------");
                })
            })
        })
    })

    it("53. Handling Alerts", () => {
        browser.waitForAngularEnabled(false);
        browser.get("https://rahulshettyacademy.com/AutomationPractice/",5000)
        element(by.id("name")).sendKeys("MiTest")
        element(by.id("alertbtn")).click()
        browser.sleep(1000)
        browser.switchTo().alert().accept()
        element(by.id("confirmbtn")).click()
        browser.sleep(1000)
        browser.switchTo().alert().dismiss()
        browser.sleep(1000)
    })

})