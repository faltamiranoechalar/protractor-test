"use strict";
exports.__esModule = true;
var protractor_1 = require("protractor");
function validateElementIsPresent(byFinder, searchResult) {
    protractor_1.element(byFinder).isPresent().then(function (val) {
        expect(val).toBe(searchResult, "isElementPresent() error: " + byFinder);
    });
}
function addItemToCart(itemNameSearched) {
    //Buscar entre los cards un item
    protractor_1.element.all(protractor_1.by.xpath(".//app-card")).each(function (itemCard) {
        itemCard.element(protractor_1.by.xpath(".//h4[@class='card-title']/a")).getText().then(function (itemCardText) {
            if (itemCardText == itemNameSearched) {
                itemCard.element(protractor_1.by.className("btn btn-info")).click();
            }
        });
    });
}
function removeItemFromCart(itemNameSearched) {
    //Buscar entre los cards un item
    protractor_1.element.all(protractor_1.by.xpath(".//table//tr[td[contains(@class,'col')]]")).each(function (itemCard) {
        itemCard.element(protractor_1.by.xpath(".//h4/a")).getText().then(function (itemCardText) {
            if (itemCardText == itemNameSearched) {
                itemCard.element(protractor_1.by.className("btn btn-danger")).click();
            }
        });
    });
}
function verifyItemsCountInCart(expectedItemCount) {
    protractor_1.element.all(protractor_1.by.xpath(".//table//tr[td[contains(@class,'col')]]")).count().then(function (itemsCount) {
        expect(itemsCount).toBe(expectedItemCount, "Items count different than expected");
    });
}
describe(("10.Real Time project"), function () {
    xit("- 58. Validating errors", function () {
        protractor_1.browser.get("https://qaclickacademy.github.io/protocommerce/", 5000);
        //la app esta mal porque deberia mostrar un alert 
        //cuando se hace click en el boton y no hay datos 
        //pero no muestra nada y el expect de abajo es FALSE
        protractor_1.element(protractor_1.by.xpath(".//input[@class='btn btn-success']")).click();
        validateElementIsPresent(protractor_1.by.className("alert alert-danger"), false);
        protractor_1.element(protractor_1.by.xpath(".//input[@name='name' and @required]")).sendKeys("my name");
        protractor_1.element(protractor_1.by.name("email")).sendKeys("a@a.com");
        protractor_1.element(protractor_1.by.id("exampleInputPassword1")).sendKeys("password123");
        protractor_1.element(protractor_1.by.id("exampleCheck1")).click();
        protractor_1.element(protractor_1.by.id("exampleFormControlSelect1")).click();
        protractor_1.element(protractor_1.by.xpath(".//option[text()='Female']")).click();
        protractor_1.element.all(protractor_1.by.name("inlineRadioOptions")).first().click();
        protractor_1.element(protractor_1.by.xpath(".//input[@class='btn btn-success']")).click();
        //la app muestra success cuando se hace click en el boton
        expect((protractor_1.element(protractor_1.by.className("alert alert-success alert-dismissible"))).getText()).toContain("The Form has been submitted successfully!");
        //este es ERROR VOLUNTARIO para validar 
        //funcionamiento de validateElementIsPresent()
        validateElementIsPresent(protractor_1.by.className("xyz"), true);
        protractor_1.browser.sleep(3000);
    });
    it("- 64. String functions to extract count of cart ítems", function () {
        protractor_1.browser.get("https://qaclickacademy.github.io/protocommerce/", 3000);
        protractor_1.element(protractor_1.by.linkText("Shop")).click();
        //Validar q usuario fue direccionado a pag correcta
        validateElementIsPresent(protractor_1.by.xpath(".//h1[text()='Shop Name']"), true);
        addItemToCart("iphone X");
        expect(protractor_1.element(protractor_1.by.className("nav-link btn btn-primary")).getText()).toContain("Checkout ( 1 )");
        addItemToCart("Blackberry");
        expect(protractor_1.element(protractor_1.by.className("nav-link btn btn-primary")).getText()).toContain("Checkout ( 2 )");
        protractor_1.element(protractor_1.by.className("nav-link btn btn-primary")).click();
        //Validar q usuario fue direccionado a pag correcta
        validateElementIsPresent(protractor_1.by.className("table table-hover"), true);
        verifyItemsCountInCart(2);
        removeItemFromCart("Blackberry");
        verifyItemsCountInCart(1);
        protractor_1.browser.sleep(5000);
    });
});
