export class ClassDemo {
    userName:string;
    firstName:string;
    lastName:string;
    
    constructor(uName:string){
        this.userName="DefaultUserName" + uName
    }

    public get getFirstName() : string {
        return this.firstName
    }
    
    public set setFirstName(v : string) {
        this.firstName = v;
    }

    public get getLastName() : string {
        return this.lastName;
    }
    
    public set setLastName(v : string) {
        this.lastName = v;
    }
    
    public get getUserName() : string {
        return this.userName;
    }
    
    public set setUserName(v : string) {
        this.userName = v;
    }
}