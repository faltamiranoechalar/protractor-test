"use strict";
exports.__esModule = true;
var protractor_1 = require("protractor");
describe('Spec 8 Handle actions - ', function () {
    var until = protractor_1.ExpectedConditions;
    xit(("48. Actions and its importance"), function () {
        protractor_1.browser.waitForAngularEnabled(false);
        protractor_1.browser.get("https://rahulshettyacademy.com/AutomationPractice/", 5000);
        protractor_1.browser.actions().mouseMove(protractor_1.element(protractor_1.by.id("mousehover"))).perform();
        protractor_1.browser.actions().mouseMove(protractor_1.element(protractor_1.by.linkText("Top"))).click().perform().then(function () {
            protractor_1.browser.sleep(3000); //sleep to see actions
        });
        protractor_1.browser.actions().mouseMove(protractor_1.element(protractor_1.by.id("mousehover"))).perform();
        protractor_1.browser.actions().mouseMove(protractor_1.element(protractor_1.by.linkText("Reload"))).click().perform();
        protractor_1.browser.sleep(3000);
    });
    xit(("49. Handling autocomplete dropdowns"), function () {
        protractor_1.browser.get("https://material.angularjs.org/latest/demo/autocomplete", 5000);
        protractor_1.element(protractor_1.by.model("$mdAutocompleteCtrl.scope.searchText")).sendKeys("Ma");
        protractor_1.element.all(protractor_1.by.xpath(".//ul[@class='md-autocomplete-suggestions']/li")).
            each(function (item) {
            item.getText().then(function (itemText) {
                if (itemText == "Maryland") {
                    item.click();
                }
            });
        });
        expect(protractor_1.element(protractor_1.by.model("$mdAutocompleteCtrl.scope.searchText")).getAttribute("value")).toMatch("Maryland");
        protractor_1.browser.sleep(3000);
    });
    xit("50. Switch to new tabs", function () {
        protractor_1.browser.get("http://posse.com/");
        protractor_1.element(protractor_1.by.xpath(".//span[@class='location' and contains(text(),'LONDON')]")).click();
        protractor_1.browser.wait(until.presenceOf(protractor_1.element(protractor_1.by.xpath(".//a[@class='makeInstrumented' and @ng-href]//span[text()='Borough Market']"))), 4000);
        protractor_1.element(protractor_1.by.xpath(".//a[@class='makeInstrumented' and @ng-href]//span[text()='Borough Market']")).click().then(function () {
            protractor_1.browser.getAllWindowHandles().then(function (windowHandles) {
                protractor_1.browser.getTitle().then(function (title) {
                    console.log("Title before:" + title + "------------------------");
                });
                protractor_1.browser.switchTo().window(windowHandles[1]);
                protractor_1.browser.getTitle().then(function (title) {
                    console.log("Title after:" + title + "------------------------");
                });
                protractor_1.browser.switchTo().window(windowHandles[0]);
                protractor_1.browser.getTitle().then(function (title) {
                    console.log("Title final:" + title + "------------------------");
                });
            });
        });
    });
    it("53. Handling Alerts", function () {
        protractor_1.browser.waitForAngularEnabled(false);
        protractor_1.browser.get("https://rahulshettyacademy.com/AutomationPractice/", 5000);
        protractor_1.element(protractor_1.by.id("name")).sendKeys("MiTest");
        protractor_1.element(protractor_1.by.id("alertbtn")).click();
        protractor_1.browser.sleep(1000);
        protractor_1.browser.switchTo().alert().accept();
        protractor_1.element(protractor_1.by.id("confirmbtn")).click();
        protractor_1.browser.sleep(1000);
        protractor_1.browser.switchTo().alert().dismiss();
        protractor_1.browser.sleep(1000);
    });
});
