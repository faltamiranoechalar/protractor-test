import { browser, element, by, ExpectedConditions } from "protractor";

describe('9. Frames and Sync - ', () => {
    let _until = ExpectedConditions

    it(("55. Frames"), () => {
        browser.waitForAngularEnabled(false);
        browser.get("https://rahulshettyacademy.com/AutomationPractice/",5000)
        browser.switchTo().frame(element(by.id("courses-iframe")).getWebElement());
        element(by.xpath(".//h3/span")).getText().then((TitleText) => {
            console.log(TitleText); //Prints the text of a title within the frame
        })
        browser.sleep(1000)
    })

    it(("59. Handling synchronization"), () => {
        browser.waitForAngularEnabled(false)
        browser.get("http://www.itgeared.com/demo/1506-ajax-loading.html",5000)
        element(by.linkText("Click to load get data via Ajax!")).click()
        browser.wait(_until.invisibilityOf(element(by.id("loader"))),3000)

        //Esta linea falla si el getText no retorna algo q contenga lo esperado
        //Expect() de Jasmine - ya resuelve el promise
        expect(element(by.id("results")).getText()).toContain("Process completed!")

        //Esta linea falla si el webelement con el xpath no es desplegado a tiempo
        browser.wait(_until.visibilityOf(element(by.xpath(".//div[contains(text(),'Process completed!')]"))),5000) 
    })
})