"use strict";
exports.__esModule = true;
var protractor_1 = require("protractor");
xdescribe('Describe', function () {
    beforeEach(function () {
        console.log('Before Each Called');
    });
    it('Sample 1', function () {
        console.log('First it block');
    });
    it('Sample 2', function () {
        console.log('Second It block');
    });
    afterEach(function () {
        console.log('After Each Called');
    });
});
xdescribe('Expected sum example', function () {
    it('Should return expected sum', function () {
        var a = 10;
        var b = 15;
        var c = a + b;
        expect(c).toEqual(25);
    });
    it('Expected sum negative example', function () {
        var a = 5;
        var b = 10;
        expect(a + b).not.toEqual(10);
    });
});
describe('Navigate to Google', function () {
    it(' - Should display text', function () {
        protractor_1.browser.get("https://material.angular.io/", 3000)
            .then(function () { return (protractor_1.browser.getTitle()); })
            .then(function (mytitle) { return (console.log(mytitle)); });
    });
    it(' - Restart the browser', function () {
        protractor_1.browser.manage().window().maximize();
        protractor_1.browser.get("https://material.angular.io/", 10000)
            .then(function () { return (protractor_1.browser.restart()); })
            .then(function () { return (protractor_1.browser.get("https://material.angular.io/", 10000)); });
    });
    it((" - Reopen closed browser"), function () {
        protractor_1.browser.get("https://material.angular.io/", 5000)
            .then(function () { return (console.log("Browser displayed.")); })
            .then(function () { return (protractor_1.browser.close()); })
            .then(function () { return (console.log("Browser closed.")); });
        protractor_1.browser.get("https://material.angular.io/", 10000);
    });
    it(" - Should navigate forward and back", function () {
        protractor_1.browser.get("https://material.angular.io/", 10000)
            .then(function () { return (console.log("First get")); })
            .then(function () { return (protractor_1.browser.get("https://www.protractortest.org/#/faq")); })
            .then(function () { return (console.log("Second get")); })
            .then(function () { return (protractor_1.browser.navigate().back()); })
            .then(function () { return (console.log("Navigate back")); })
            .then(function () { return (protractor_1.browser.sleep(3000)); })
            .then(function () { return (protractor_1.browser.navigate().forward()); })
            .then(function () { return (console.log("Navigate forward")); })
            .then(function () { return (protractor_1.browser.navigate().to("https://material.angular.io/")); })
            .then(function () { return (console.log("Navigate To")); });
    });
    it('Example of to method', function () {
        protractor_1.browser.get('https://material.angular.io/')
            .then(function () { return (protractor_1.browser.navigate().to("https://protractortest.org/#/faq")); }) // to() method is used here
            .then(function () { return (protractor_1.browser.sleep(5000)); });
    });
    fit(" - Return menu option text", function () {
        var autocomplete_MenuOption = protractor_1.element(protractor_1.by.xpath(".//ul[@id='docs-menu-Demos']"));
        protractor_1.browser.get("https://material.angularjs.org/latest/demo/autocomplete", 5000);
        autocomplete_MenuOption.getAttribute("innerText")
            .then(function (autoText) { return (console.log("Autocomplete text:" + autoText)); });
        expect(autocomplete_MenuOption.isPresent()).toBeTruthy("Deberia ser falso:" + autocomplete_MenuOption.locator());
    });
});
