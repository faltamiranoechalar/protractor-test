"use strict";
exports.__esModule = true;
var protractor_1 = require("protractor");
describe('Spec 9 Frames and Sync - ', function () {
    var _until = protractor_1.ExpectedConditions;
    xit(("55. Frames"), function () {
        protractor_1.browser.waitForAngularEnabled(false);
        protractor_1.browser.get("https://rahulshettyacademy.com/AutomationPractice/", 5000);
        protractor_1.browser.switchTo().frame(protractor_1.element(protractor_1.by.id("courses-iframe")).getWebElement());
        protractor_1.element(protractor_1.by.xpath(".//h3/span")).getText().then(function (TitleText) {
            console.log(TitleText); //Prints the text of a title within the frame
        });
        protractor_1.browser.sleep(1000);
    });
    it(("59. Handling synchronization"), function () {
        protractor_1.browser.waitForAngularEnabled(false);
        protractor_1.browser.get("http://www.itgeared.com/demo/1506-ajax-loading.html", 5000);
        protractor_1.element(protractor_1.by.linkText("Click to load get data via Ajax!")).click();
        protractor_1.browser.wait(_until.invisibilityOf(protractor_1.element(protractor_1.by.id("loader"))), 3000);
        expect(protractor_1.element(protractor_1.by.id("results")).getText()).toContain("My Process completed!");
        protractor_1.browser.wait(_until.visibilityOf(protractor_1.element(protractor_1.by.xpath(".//div[contains(text(),'Process completed!')]"))), 4000);
    });
});
