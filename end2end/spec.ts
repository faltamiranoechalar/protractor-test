import { browser, element, by, ElementFinder } from "protractor";

xdescribe('Describe', () => {
    beforeEach(() => { //beforeEach function
        console.log('Before Each Called');
    });
    it('Sample 1', () => {
        console.log('First it block');
    });
    it('Sample 2', () => {
        console.log('Second It block');
    });
    afterEach(() => { //afterEach function
        console.log('After Each Called');
    });
});

xdescribe('Expected sum example', () => {
    it('Should return expected sum', () => {
        let a = 10;
        let b = 15;
        let c = a + b;
        expect(c).toEqual(25);
    })

    it('Expected sum negative example', () => {
        let a = 5;
        let b = 10;
        expect(a + b).not.toEqual(10);
    })
});

describe('Navigate to Google', () => {
    it(' - Should display text', () => {
        browser.get("https://material.angular.io/", 3000)
            .then(() => (browser.getTitle()))
            .then((mytitle) => (console.log(mytitle)));
    });

    it(' - Restart the browser', () => {
        browser.manage().window().maximize();
        browser.get("https://material.angular.io/", 10000)
            .then(() => (browser.restart()))
            .then(() => (browser.get("https://material.angular.io/", 10000)));
    });

    it((" - Reopen closed browser"), () => {
        browser.get("https://material.angular.io/", 5000)
            .then(() => (console.log("Browser displayed.")))
            .then(() => (browser.close()))
            .then(() => (console.log("Browser closed.")));
        browser.get("https://material.angular.io/", 10000);
    });

    it(" - Should navigate forward and back", () => {
        browser.get("https://material.angular.io/", 10000)
            .then(() => (console.log("First get")))
            .then(() => (browser.get("https://www.protractortest.org/#/faq")))
            .then(() => (console.log("Second get")))
            .then(() => (browser.navigate().back()))
            .then(() => (console.log("Navigate back")))
            .then(() => (browser.sleep(3000)))
            .then(() => (browser.navigate().forward()))
            .then(() => (console.log("Navigate forward")))
            .then(() => (browser.navigate().to("https://material.angular.io/")))
            .then(() => (console.log("Navigate To")));
    });

    it('Example of to method', () => {
        browser.get('https://material.angular.io/')
            .then(() => (browser.navigate().to("https://protractortest.org/#/faq"))) // to() method is used here
            .then(() => (browser.sleep(5000)));
    });

    fit(" - Return menu option text", () => {
        let autocomplete_MenuOption: ElementFinder = element(by.xpath(".//ul[@id='docs-menu-Demos']"));

        browser.get("https://material.angularjs.org/latest/demo/autocomplete", 5000);
        autocomplete_MenuOption.getAttribute("innerText")
            .then((autoText) => (console.log("Autocomplete text:" + autoText)));
            expect(autocomplete_MenuOption.isPresent()).toBeTruthy("Deberia ser falso:" + autocomplete_MenuOption.locator());
    })
})