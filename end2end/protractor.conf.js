exports.config = {
    framework: 'jasmine', //Type of Framework used 
    directConnect: true,
    specs: ['spec10RealTimeProject.js'], //Name of the Specfiles
    // specs: ['spec10RealTimeProject.js','spec9FramesAndSync.js'], //Name of the Specfiles
    // capabilities: {
    //     'browserName': 'firefox',
    //     'moz:firefoxOptions': {
    //         'args': ['--safe-mode']
    //     }
    // },
    resultJsonOutputFile:'./testResults.json',
    onPrepare() {
        browser.driver.manage().window().maximize();
        require('ts-node').register({
            project: require('path').join(__dirname, './tsconfig.json') // Relative path of tsconfig.json file 
        });
    },
      // Options to be passed to Jasmine-node.
    jasmineNodeOpts: {
        showColors: true, // Use colors in the command line report.
    }
}