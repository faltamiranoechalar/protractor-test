import { browser, element, by, ExpectedConditions } from "protractor";

function validateElementIsPresent(byFinder,searchResult){
    element(byFinder).isPresent().then((val) => {
            expect(val).toBe(searchResult,"isElementPresent() error: " + byFinder);
    })
}

function addItemToCart(itemNameSearched) {
    //Buscar entre los cards un item
    element.all(by.xpath(".//app-card")).each((itemCard) => {
        itemCard.element(by.xpath(".//h4[@class='card-title']/a")).getText().then((itemCardText) => {
            if (itemCardText==itemNameSearched){
                itemCard.element(by.className("btn btn-info")).click();
            }
        })
    })
}

function removeItemFromCart(itemNameSearched) {
    //Buscar entre los cards un item
    element.all(by.xpath(".//table//tr[td[contains(@class,'col')]]")).each((itemCard) => {
        itemCard.element(by.xpath(".//h4/a")).getText().then((itemCardText) => {
            if (itemCardText==itemNameSearched){
                itemCard.element(by.className("btn btn-danger")).click();
            }
        })
    })
}

function verifyItemsCountInCart(expectedItemCount){
    element.all(by.xpath(".//table//tr[td[contains(@class,'col')]]")).count().then((itemsCount) => {
            expect(itemsCount).toBe(expectedItemCount,"Items count different than expected")
        })
}

describe(("10.Real Time project"), () => {
    xit("- 58. Validating errors", () => {
        browser.get("https://qaclickacademy.github.io/protocommerce/", 5000);
        
        //la app esta mal porque deberia mostrar un alert 
        //cuando se hace click en el boton y no hay datos 
        //pero no muestra nada y el expect de abajo es FALSE
        element(by.xpath(".//input[@class='btn btn-success']")).click();
        validateElementIsPresent(by.className("alert alert-danger"),false)

        element(by.xpath(".//input[@name='name' and @required]")).sendKeys("my name");
        element(by.name("email")).sendKeys("a@a.com");
        element(by.id("exampleInputPassword1")).sendKeys("password123");
        element(by.id("exampleCheck1")).click();
        element(by.id("exampleFormControlSelect1")).click();
        element(by.xpath(".//option[text()='Female']")).click();
        element.all(by.name("inlineRadioOptions")).first().click();
        element(by.xpath(".//input[@class='btn btn-success']")).click();

        //la app muestra success cuando se hace click en el boton
        expect((element(by.className("alert alert-success alert-dismissible"))).getText()).toContain("The Form has been submitted successfully!")
        //este es ERROR VOLUNTARIO para validar 
        //funcionamiento de validateElementIsPresent()
        validateElementIsPresent(by.className("xyz"),true)
        browser.sleep(3000)
    })

    it("- 64. String functions to extract count of cart ítems", () => {
        browser.get("https://qaclickacademy.github.io/protocommerce/",3000);
        element(by.linkText("Shop")).click();

        //Validar q usuario fue direccionado a pag correcta
        validateElementIsPresent(by.xpath(".//h1[text()='Shop Name']"),true)

        addItemToCart("iphone X")
        expect(element(by.className("nav-link btn btn-primary")).getText()).toContain("Checkout ( 1 )")

        addItemToCart("Blackberry")
        expect(element(by.className("nav-link btn btn-primary")).getText()).toContain("Checkout ( 2 )")

        element(by.className("nav-link btn btn-primary")).click();

        //Validar q usuario fue direccionado a pag correcta
        validateElementIsPresent(by.className("table table-hover"),true)

        verifyItemsCountInCart(2)

        removeItemFromCart("Blackberry")

        verifyItemsCountInCart(1)

        browser.sleep(5000);
    })
})



