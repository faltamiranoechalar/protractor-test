import { browser, element, by, ElementFinder } from "protractor";

describe('Spec 7 Deep drive into Protractor - ', () => {
    xit("40. Should use chain locators", () => {

        function AddNumbers(a,b)
        {
            element(by.model("first")).sendKeys(a);
            element(by.model("second")).sendKeys(b);
            element(by.id("gobutton")).click();
        }
        
        browser.get("http://localhost:3456/",5000);
        AddNumbers(1,0);
        AddNumbers(2,0);
        AddNumbers(3,0);
        AddNumbers(4,0);
        AddNumbers(5,0);
        element.all(by.repeater("result in memory")).each((item) => {
            item.element(by.xpath("./td[3]")).getText().then((resultText) => {
                console.log(resultText)
            })
        })
    })

    it("46. Handling list of dropdown options with example", () => {

        function AddNumbers(operA,operB,oper)
        {
            var numOpera=0;
            element(by.model("first")).sendKeys(operA);
            element(by.model("second")).sendKeys(operB);

            element.all(by.tagName("option")).each((operItem) => {
                operItem.getAttribute("value").then((operValue) => {
                    if(operValue==oper){
                        operItem.click();
                    }
                })
            })
            element(by.id("gobutton")).click();
        }
        
        browser.get("http://localhost:3456/",5000);
        AddNumbers(1,0,"ADDITION");
        AddNumbers(2,0,"MULTIPLICATION");
        AddNumbers(3,0,"SUBTRACTION");
        element.all(by.repeater("result in memory")).each((item) => {
            item.element(by.xpath("./td[3]")).getText().then((resultText) => {
                console.log(resultText)
            })
        })
    })
})