import { Config } from "protractor";

export let config: Config = {
  // The address of a running selenium server.
  // seleniumAddress: 'http://localhost:4444/wd/hub',
  directConnect: true,
  resultJsonOutputFile:'./results/testResults.json',
  // Capabilities to be passed to the webdriver instance.
  capabilities: {
    browserName: 'chrome'
    // browserName: 'firefox',
    //   'moz:firefoxOptions': {
    //         'args': ['--safe-mode']
    //   }
  },
  // Spec patterns are relative to the configuration file location passed
  // to protractor (in this example conf.js).
  // They may include glob patterns.
  specs: ['spec14ProtractorTypescript.js'],

  // Options to be passed to Jasmine-node.
  jasmineNodeOpts: {
    showColors: true, // Use colors in the command line report.
  }
};
