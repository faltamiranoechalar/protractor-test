Feature: Calculator
    This feature will implemet all TCs to validate a Calculator

    @calculator
    Scenario: Add functionality testing
        Given I navigate to calculator page
        Then I enter "3" value in first textbox
        And I enter "11" value in second textbox
        And I click on 'Go' button
        Then result "14" should be displayed

    Scenario Outline: Add multiple attempts testing
        Given I navigate to calculator page
        Then I enter "<value1>" value in first textbox
        And I enter "<value2>" value in second textbox
        And I click on 'Go' button
        Then result "<result>" should be displayed
        Examples:
            | value1 | value2 | result |
            | 3      | 6      | 8      |
            | 6      | 15     | 20     |