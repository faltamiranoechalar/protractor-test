import { ElementFinder, element, by } from "protractor";

export class CalculatorLandingPage
{
    first_editBox: ElementFinder;
    second_editBox: ElementFinder;
    go_button: ElementFinder;
    result_text: ElementFinder;

    constructor()
    {
        this.first_editBox = element(by.model("first"));
        this.second_editBox = element(by.model("second"));
        this.go_button = element(by.id("gobutton"));
        this.result_text = element(by.xpath(".//tr/td[3]"));
    }
}