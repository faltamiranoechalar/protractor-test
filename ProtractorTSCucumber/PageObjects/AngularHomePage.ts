import { ElementFinder, element, by } from "protractor";

export class AngularHomePage
{
    angular_link: ElementFinder;
    search_textbox: ElementFinder;

    constructor()
    {
        this.angular_link = element(by.linkText("angular.io"));
        this.search_textbox = element(by.css("input[type='search']"));
    }
}