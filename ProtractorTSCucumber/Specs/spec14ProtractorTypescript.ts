import { browser, element, by } from "protractor";
import { CalculatorLandingPage } from "../PageObjects/CalculatorLandingPage";

describe(("14.Protractor With Typescript"), () => {
    xit("- 82. Importance of Async Await", async() => {
        browser.get("https://juliemr.github.io/protractor-demo/")
        await element(by.model("first")).sendKeys("1");
        await element(by.model("second")).sendKeys("5");
        await element(by.id("gobutton")).click();
    })

    it("- 83. Importance of PageObject mechanism", async() => {
        browser.get("https://juliemr.github.io/protractor-demo/")
        let calcPage =  new CalculatorLandingPage()
        await calcPage.first_editBox.sendKeys("10")
        await calcPage.second_editBox.sendKeys("20")
        calcPage.go_button.click()
        expect(calcPage.result_text.getText()).toEqual("30","Result incorrect.")
    })
})