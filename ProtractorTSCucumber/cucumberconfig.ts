import { Config } from "protractor";
import * as reporter from "cucumber-html-reporter";

export let config: Config = {
  directConnect: true,
  framework: 'custom',
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  resultJsonOutputFile: './output/protractor_report.json',
  capabilities: {
    browserName: 'chrome'
    // browserName: 'firefox',
    //   'moz:firefoxOptions': {
    //         'args': ['--safe-mode']
    //   }
  },
  specs: ['../Features/*.feature'],
  cucumberOpts: {
    // tags: "@smoketest or @calculator",
    format: "json:./output/cucumber_report.json",
    require: [
      './stepDefinitions/*.js'
    ],
  },
  onComplete: () => {
    var options = {
      theme: 'bootstrap',
      jsonFile: './output/cucumber_report.json',
      output: './output/cucumber_report.html',
      reportSuiteAsScenarios: true,
      launchReport: true,
      name: "My Protractor Cucumber tests",
      brandTitle: "Smoke Tests Report",
      storeScreenshots: true,
      columnLayout: 1,
      metadata: {
        "App Version": "0.3.2",
        "Test Environment": "STAGING",
        "Browser": "Chrome  54.0.2840.98",
        "Platform": "Windows 10",
        "Parallel": "Scenarios",
        "Executed": "Remote"
      }
    };
    reporter.generate(options);
  }
};
