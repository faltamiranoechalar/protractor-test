import { Given, When } from "cucumber";
import { AngularHomePage } from "../PageObjects/AngularHomePage"
import { browser } from "protractor";

let angularPage =  new AngularHomePage()

Given('user login to Angular page', async function () {
    await browser.get("https://angularjs.org/")
});

When('I click on \'angular.io\' link in Landing page', async function () {
    await angularPage.angular_link.click();
});