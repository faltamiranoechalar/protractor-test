import { Before, After, BeforeAll, AfterAll, Status } from "cucumber";
import { browser } from "protractor";

BeforeAll(function () {
    console.log("BeforeAll...")
})

Before({ tags: "@smoketest" }, function () {
    console.log("Before hook @smoketest");
    browser.manage().window().maximize();
})

Before(function () {
    console.log("Before hook");
})

After({ tags: "@smoketest" }, function () {
    console.log("After hook @smoketest");
})

After(async function (scenario) {
    console.log("Test is completed");
    if (scenario.result.status === Status.FAILED) {
        const screenshot = await browser.takeScreenshot();
        this.attach(screenshot, "image/png");
    }
});

AfterAll(function () {
    console.log("afterAll...")
})
