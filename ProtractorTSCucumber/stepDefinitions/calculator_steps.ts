import { Given, Then } from "cucumber";
import { browser } from "protractor";
import { CalculatorLandingPage } from "../PageObjects/CalculatorLandingPage";

let calcPage = new CalculatorLandingPage();
var chexpect = require('chai').expect

Given('I navigate to calculator page', async function() {
    await browser.get("https://juliemr.github.io/protractor-demo/")
});

Then('I enter {string} value in first textbox', async function(string) {
    await calcPage.first_editBox.sendKeys(string)
});

Then('I enter {string} value in second textbox', async function(string) {
    await calcPage.second_editBox.sendKeys(string)
});

Then('I click on \'Go\' button', async function () {
    await calcPage.go_button.click()
});

Then('result {string} should be displayed', async function (string) {
    await calcPage.result_text.getText().then(function(resultText){
        chexpect(resultText).to.equal(string);
    })
});